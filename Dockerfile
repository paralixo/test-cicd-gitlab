FROM alpine

RUN apk update \
&& apk add nodejs nodejs-npm

ADD . /app/
WORKDIR /app
RUN npm install

EXPOSE 8081

CMD npm run start
